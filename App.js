import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import Result from './src/components/result';
import Calculator from './src/components/calculator';
import Button from './src/components/buttons';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
const initialState = {
  resultText: '',
  calculatorText: 0,
};

const reducer = (state = initialState, action) => {
  function validate() {
    let check = state.resultText.slice(-1);
    if (['+', '-', '*', '/'].indexOf(check) >= 0) {
      console.log(state.resultText);
      return false;
    }
    return true;
  }

  switch (action.type) {
    case 'NUMS':
      if (state.calculatorText != 0 && validate() == false) {
        return {
          resultText: state.resultText + action.value,
          calculatorText: state.calculatorText,
        };
      }
      if (
        validate() == true &&
        eval(state.resultText) == state.calculatorText
      ) {
        return {
          resultText: action.value + '',
          calculatorText: state.calculatorText,
        };
      }
      return {
        resultText: state.resultText + action.value,
        calculatorText: state.calculatorText,
      };
    case 'C':
      return {
        resultText: state.resultText.slice(0, -1),
        calculatorText: state.calculatorText,
      };
    case 'DEL':
      return {
        resultText: (state.resultText = ''),
        calculatorText: state.calculatorText,
      };
    case 'OPS':
      let lastChar = state.resultText.split('').pop();
      if (['+', '-', '*', '/'].indexOf(lastChar) >= 0) {
        return {
          resultText: state.resultText,
          calculatorText: state.calculatorText,
        };
      }
      if (state.calculatorText == 0) {
        return {
          resultText: state.resultText + action.value,
          calculatorText: state.calculatorText,
        };
      }
      if (
        validate() == true &&
        eval(state.resultText) == state.calculatorText
      ) {
        return {
          resultText: state.calculatorText + action.value,
          calculatorText: state.calculatorText,
        };
      }
      return {
        resultText: state.resultText + action.value,
        calculatorText: state.calculatorText,
      };

    case 'CAL':
      if (validate() == false) {
        return {
          resultText: state.resultText,
          calculatorText: state.calculatorText,
        };
      }
      return {
        calculatorText: eval(state.resultText),
        resultText: state.resultText,
      };
    default:
      return state;
  }
};

const store = createStore(reducer);

export default class App extends Component {
  constructor() {
    super();
    this.operations = ['+', '-', '*', '/'];
  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Result />
          <Calculator />
          <Button />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
