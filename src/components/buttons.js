import React, {Component} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
class Button extends Component {
  constructor() {
    super();
    this.operations = ['C', 'DEL', '+', '-', '*', '/'];
  }

  buttonPressed(text) {
    if (text == '=') {
      this.props.dispatch({type: 'CAL'});
    } else {
      this.props.dispatch({type: 'NUMS', value: text});
    }
  }

  operate(ops) {
    if (ops == 'DEL') {
      this.props.dispatch({type: 'DEL'});
    } else if (ops == 'C') {
      this.props.dispatch({type: 'C'});
    } else {
      this.props.dispatch({type: 'OPS', value: ops});
    }
  }

  render() {
    let rows = [];
    let nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9], ['.', 0, '=']];
    for (let i = 0; i < 4; i++) {
      let row = [];
      for (let j = 0; j < 3; j++) {
        row.push(
          <TouchableOpacity
            key={nums[i][j]}
            style={styles.btn}
            onPress={() => this.buttonPressed(nums[i][j])}>
            <Text style={styles.btnText}>{nums[i][j]}</Text>
          </TouchableOpacity>,
        );
      }
      rows.push(
        <View key={i} style={styles.row}>
          {row}
        </View>,
      );
    }

    let ops = [];
    for (let i = 0; i < 6; i++) {
      ops.push(
        <TouchableOpacity
          key={this.operations[i]}
          style={styles.btn}
          onPress={() => this.operate(this.operations[i])}>
          <Text style={(styles.btnText, styles.white)}>
            {this.operations[i]}
          </Text>
        </TouchableOpacity>,
      );
    }

    return (
      <View style={styles.buttons}>
        <View style={styles.numbers}>{rows}</View>
        <View style={styles.operations}>{ops}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  btnText: {
    fontSize: 30,
    color: 'white',
  },

  white: {
    color: 'white',
    fontSize: 30,
  },

  btn: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
  },

  buttons: {
    flex: 7,
    flexDirection: 'row',
  },

  numbers: {
    flex: 3,
    backgroundColor: '#434343',
  },

  operations: {
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: '#636363',
  },
});

export default connect()(Button);
