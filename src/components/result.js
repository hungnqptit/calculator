import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
class Result extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <View style={styles.result}>
        <Text style={styles.resultText}>{this.props.resultText}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  result: {
    flex: 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  resultText: {
    fontSize: 30,
  },
});

function mapStateToProp(state) {
  return {
    resultText: state.resultText,
  };
}

export default connect(mapStateToProp)(Result);
