import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
class Calculator extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <View style={styles.calculator}>
        <Text style={styles.calculatorText}>{this.props.calculatorText}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calculatorText: {
    fontSize: 24,
  },

  calculator: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});

function mapStateToProp(state) {
  return {
    calculatorText: state.calculatorText,
  };
}

export default connect(mapStateToProp)(Calculator);
